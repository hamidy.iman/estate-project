﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using Estate.Shared.Resources;

namespace Estate.Shared.Enums
{
   public static class EnumExtensions
    {


        private static string GetDisplayName(this Enum enumValue)
        {
           

            var type = enumValue.GetType();
            var member = type.GetMember(enumValue.ToString());
            DisplayAttribute displayName = (DisplayAttribute)member[0].GetCustomAttributes(typeof(DisplayAttribute), false).FirstOrDefault();

            return displayName.Name;
        }

        public static string GetDisplayValue(this Enum enumValue, Type type)
        {
            var displayName = GetDisplayName(enumValue);
            ResourceManager temp = new System.Resources.ResourceManager(type.FullName, type.Assembly);
            var str= temp.GetString(displayName, CultureInfo.CurrentCulture);
            return str;
        }

    }
}
