﻿using System.ComponentModel.DataAnnotations;
using Estate.Shared.Resources;

namespace Estate.Shared.Enums
{
    public class Enums
    {

        public enum EstateLocation
        {
            [Display(Name = "Northern", ResourceType = typeof(Messages))]
            Northern = 1,
            [Display(Name = "Southern", ResourceType = typeof(Messages))]
            Southern = 2,
        }

        public enum ResponseStatusCodeType
        {
            [Display(Name = "CreatedApiResult", ResourceType = typeof(Messages))]
            Created = 201,
            [Display(Name = "AcceptedApiResult", ResourceType = typeof(Messages))]
            Accepted = 202,
            [Display(Name = "NoContentApiResult", ResourceType = typeof(Messages))]
            NoContent = 204,
            [Display(Name = "NotFoundApiResult", ResourceType = typeof(Messages))]
            NotFound = 404,
            [Display(Name = "OkApiResult", ResourceType = typeof(Messages))]
            Ok = 200,
            [Display(Name = "BadRequestApiResult", ResourceType = typeof(Messages))]
            BadRequest = 400,
            [Display(Name = "ServerErrorApiResult", ResourceType = typeof(Messages))]
            ServerError = 500

        }

    }
}
