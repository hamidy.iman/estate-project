﻿using Estate.Shared.Resources;
using Estate.Shared.Utilities;

namespace Estate.Shared.DataAnnotations
{
    /// <summary>
    /// Validate persian phone number format
    /// </summary>
    public class PersianPhoneNumberValidatorAttribute : System.ComponentModel.DataAnnotations.ValidationAttribute
    {
        public PersianPhoneNumberValidatorAttribute() : base()
        {
        }

        /// <summary>Checks that the value of the data field is valid.</summary>
        /// <param name="value">The data field value to validate.</param>
        /// <returns>true always.</returns>
        public override bool IsValid(object value)
        {
            var val = value?.ToString();
            if (val == null || val.Trim().Length == 0)
                return true;
            else
            {
                return val.IsValidIranianPhoneNumber();
            }
        }

        public override string FormatErrorMessage(string name)
        {
            this.EnsureErrorMessageResource();
            return base.FormatErrorMessage(name);
        }

        public void EnsureErrorMessageResource()
        {
            if (this.ErrorMessageResourceType == null)
            {
                this.ErrorMessageResourceType = typeof(ValidationMessages);
            }
            if (string.IsNullOrEmpty(this.ErrorMessageResourceName))
            {
                this.ErrorMessageResourceName = "InvalidPhoneNumberAttribute";
            }
        }

    }
}