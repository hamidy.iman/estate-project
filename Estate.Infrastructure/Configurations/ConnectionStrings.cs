﻿namespace Estate.Infrastructure.Configurations
{
    public interface IConnectionStrings
    {
        string SqlServerDefault { get; set; }
    }

    public class ConnectionStrings : IConnectionStrings
    {
        public string SqlServerDefault { get; set; }

    }
}
