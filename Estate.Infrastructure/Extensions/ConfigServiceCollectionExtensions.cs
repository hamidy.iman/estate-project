﻿using System;
using System.Globalization;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Estate.Infrastructure.Configurations;
using Estate.Infrastructure.DataModels.Repositories;
using Estate.Infrastructure.DataModels.Repositories.Base;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace Estate.Infrastructure.Extensions
{
    public static class ConfigServiceCollectionExtensions
    {
        public static IServiceCollection AddConfiguration(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<ConnectionStrings>(config.GetSection("ConnectionStrings"));

            services.TryAddSingleton<IConnectionStrings>(sp =>
                sp.GetRequiredService<IOptions<ConnectionStrings>>().Value); // forwarding via implementation factory

            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), (typeof(Repository<,>)));
            services.AddScoped(typeof(IBaseRepository<>), (typeof(BaseRepository<>)));
            services.AddTransient<IEstateRepository, EstateRepository>();
            services.AddTransient<IEstateOwnerRepository, EstateOwnerRepository>();
            return services;
        }

       


    }
}
