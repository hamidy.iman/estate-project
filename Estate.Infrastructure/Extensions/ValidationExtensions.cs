﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Estate.Infrastructure.Extensions
{
    public static class ValidationExtensions
    {
        public static string GetValidationErrors(this DbContext context)
        {
            var errors = new StringBuilder();
            var entities = context.ChangeTracker.Entries()
                                        .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified)
                                        .Select(e => e.Entity);
            foreach (var entity in entities)
            {
                Validate(entity, errors);
            }

            return errors.ToString();
        }

        public static void Validate(object entity, StringBuilder errors)
        {
            var validationContext = new ValidationContext(entity, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(entity, validationContext, validationResults, true);

            //Group validation results by property names
            var resultsByPropNames = from res in validationResults
                                     from mname in res.MemberNames
                                     group res by mname into g
                                     select g;

            //add _errors to dictionary and inform binding engine about _errors
            foreach (var prop in resultsByPropNames)
            {
                var messages = prop.Select(r => r.ErrorMessage).ToList();
                var names = prop.Key;

                errors.AppendFormat("{0}: {1}", names, messages);
            }
        }
    }
}