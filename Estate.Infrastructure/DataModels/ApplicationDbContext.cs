﻿using Estate.Infrastructure.DataModels.Entities;
using Estate.Infrastructure.DataModels.Entities.Base;
using Estate.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Estate.Infrastructure.DataModels
{
   public partial class ApplicationDbContext : DbContext
    {


        public virtual DbSet<Entities.Estate> Estates { get; set; }
        public virtual DbSet<EstateOwner> EstateOwners { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

           
           var assemblyofentities = typeof(ApplicationDbContext).Assembly;

           builder.RegisterAllEntities<IDbEntity>(assemblyofentities);
          // builder.RegisterAllEntities<IEquatable<int>>(assemblyofentities);
           builder.RegisterEntityTypeConfiguration(assemblyofentities);
           builder.AddRestrictDeleteBehaviorConvention();
           builder.AddSequentialGuidForIdConvention();
           builder.AddPluralizingTableNameConvention();



            //this.Database.EnsureCreated();
        }

       

    }
}
