﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Estate.Infrastructure.DataModels.Entities.Base;
using Estate.Shared.DataAnnotations;
using Estate.Shared.Enums;
using Estate.Shared.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Estate.Infrastructure.DataModels.Entities
{
    public class EstateOwner:BaseEntity
    {

        [Required(ErrorMessageResourceName = "FieldRequiredError",ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(128, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Name { get; set; }


        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(8, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Family { get; set; }

        [StringLength(512, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [PersianMobileNumberValidator(ErrorMessageResourceName = "MobileNumberFormatError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string MobileNumber { get; set; }

        [StringLength(512, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [PersianPhoneNumberValidator(ErrorMessageResourceName = "PhoneNumberFormatError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string PhoneNumber { get; set; }

        // Navigations

        public virtual ICollection<Estate> Estates { get; set; }


    }

    public class EstateOwnerMapping : IEntityTypeConfiguration<EstateOwner>
    {
        public void Configure(EntityTypeBuilder<EstateOwner> builder)
        {
            builder.ToTable("EstateOwners")
                .HasIndex(p => p.MobileNumber)
                .IsUnique();
        }
    }
}
