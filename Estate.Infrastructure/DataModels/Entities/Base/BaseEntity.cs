﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Estate.Shared.Resources;

namespace Estate.Infrastructure.DataModels.Entities.Base
{
    public abstract class BaseEntity:BaseEntity<Guid>
    {
       
        
        public BaseEntity()
        {
            Id = Guid.NewGuid(); 
        }
    }

    public abstract class BaseEntity<TKey>:IDbEntity
    {

        [Key]
        [Display(Name = "Id", ResourceType = typeof(Messages))]
        public TKey Id { set; get; }
    }
}
