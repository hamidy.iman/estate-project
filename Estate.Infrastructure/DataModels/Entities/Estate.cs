﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Estate.Infrastructure.DataModels.Entities.Base;
using Estate.Shared.Enums;
using Estate.Shared.Resources;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Estate.Infrastructure.DataModels.Entities
{
    public class Estate:BaseEntity
    {

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(8, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Number { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError",ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(128, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Range(minimum: 0, maximum: Double.MaxValue, ErrorMessageResourceName = "NumberRangeError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public double Area { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public Enums.EstateLocation Location { get; set; }

        [StringLength(512, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Address { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public Guid OwnerId { get; set; }

        // Navigations

        [ForeignKey(nameof(OwnerId))]
        public virtual EstateOwner EstateOwner { get; set; }

    }

    public class EstateMapping : IEntityTypeConfiguration<Estate>
    {
        public void Configure(EntityTypeBuilder<Estate> builder)
        {
            builder.ToTable("Estates")
                .HasIndex(p=>p.Number).IsUnique();
            builder.HasIndex(p=>p.Name).IsUnique();
        }
    }
}
