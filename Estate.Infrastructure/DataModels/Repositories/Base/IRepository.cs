﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Estate.Infrastructure.DataModels.Entities.Base;

namespace Estate.Infrastructure.DataModels.Repositories.Base
{
    public interface IRepository<TEntity>:IDisposable
        where TEntity : class, IDbEntity
    {
       
        IQueryable<TEntity> Table { get; }
        IQueryable<TEntity> TableNoTracking { get; }

        void Add(TEntity entity, bool saveNow = false);
        Task AddAsync(TEntity entity, CancellationToken cancellationToken, bool saveNow = false);
        void AddRange(IEnumerable<TEntity> entities, bool saveNow = false);
        Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveNow = false);
        void Attach(TEntity entity);
        void Delete(TEntity entity, bool saveNow = false);
        void Delete(object id, bool saveNow = false);
        Task DeleteAsync(TEntity entity, CancellationToken cancellationToken, bool saveNow = false);
        Task DeleteAsync(object id, CancellationToken cancellationToken, bool saveNow = false);
        void DeleteRange(IEnumerable<TEntity> entities, bool saveNow = false);
        Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveNow = false);
        void Detach(TEntity entity);
        IQueryable<TEntity> GetQuery();
        IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> predicate);
        TEntity FindByKeys(params object[] keys);
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity,bool>> filter=null,Func<IQueryable<TEntity>,IOrderedQueryable<TEntity> > orderBy=null,IEnumerable<string> includeProperties=null);
        IEnumerable<TEntity> GetAllWithIncludes(Expression<Func<TEntity,bool>> filter=null,Func<IQueryable<TEntity>,IOrderedQueryable<TEntity> > orderBy=null);
        Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken cancellationToken, Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, IEnumerable<string> includeProperties = null);
        Task<IEnumerable<TEntity>> GetAllWithIncludesAsync(CancellationToken cancellationToken, Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null, IEnumerable<string> includeProperties = null);
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null, IEnumerable<string> includeProperties = null);
        Task<TEntity> FindByKeysAsync(CancellationToken cancellationToken, params object[] keys);
        void LoadCollection<TProperty>(TEntity entity, Expression<Func<TEntity, IEnumerable<TProperty>>> collectionProperty) where TProperty : class;
        Task LoadCollectionAsync<TProperty>(TEntity entity, Expression<Func<TEntity, IEnumerable<TProperty>>> collectionProperty, CancellationToken cancellationToken) where TProperty : class;
        void LoadReference<TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> referenceProperty) where TProperty : class;
        Task LoadReferenceAsync<TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> referenceProperty, CancellationToken cancellationToken) where TProperty : class;
        void Update(TEntity entity, bool saveNow = true);
        Task UpdateAsync(TEntity entity, CancellationToken cancellationToken, bool saveNow = true);
        void UpdateRange(IEnumerable<TEntity> entities, bool saveNow = true);
        Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveNow = true);
        IEnumerable<TModel> GetListSp<TModel>(string procedureName, DynamicParameters parameters = null);
        Task<IEnumerable<TModel>> GetListSpAsync<TModel>(string procedureName, DynamicParameters parameters = null);
        void ExecuteSpWithoutReturn(string procedureName, DynamicParameters parameters = null);
        Task ExecuteSpWithoutReturnAsync(string procedureName, DynamicParameters parameters = null);
        TModel ExecuteSpScalar<TModel>(string procedureName, DynamicParameters parameters = null);
        Task<TModel> ExecuteSpScalarAsync<TModel>(string procedureName, DynamicParameters parameters = null);

    }
}