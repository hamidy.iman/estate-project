﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Estate.Infrastructure.Configurations;
using Estate.Infrastructure.DataModels.Entities.Base;
using Estate.Infrastructure.Extensions;
using Estate.Shared.Utilities;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Estate.Infrastructure.DataModels.Repositories.Base
{
    public class Repository<TEntity,TDbContext> : IRepository<TEntity>
        where TEntity : class, IDbEntity
        where TDbContext:DbContext
    {
        protected readonly TDbContext DbContext;
        private readonly IConnectionStrings ConnectionStrings;
        internal DbSet<TEntity> Entities { get; }
        public virtual IQueryable<TEntity> Table => Entities;
        public virtual IQueryable<TEntity> TableNoTracking => Entities.AsNoTracking();

        public Repository(TDbContext dbContext,IConnectionStrings connectionStrings)
        {
            DbContext = dbContext;
            Entities = DbContext.Set<TEntity>(); // City => Cities
            ConnectionStrings = connectionStrings;
        }

        #region Async Method
        public virtual async Task<TEntity> FindByKeysAsync(CancellationToken cancellationToken, params object[] keys)
        {
            return await Entities.FindAsync(keys, cancellationToken);
        }
        public async Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken cancellationToken, Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, IEnumerable<string> includeProperties = null)
        {
            IQueryable<TEntity> query = Entities;
            if (filter.IsNotNull())
            {
                query = query.Where(filter);
            }

            if (includeProperties.IsNotNull())
            {
                foreach (var property in includeProperties)
                {
                    query = query.Include(property);
                }
            }

            if (orderBy.IsNotNull())
            {
                return orderBy(query);
            }

            return await query.ToListAsync();
        }


        public async Task<IEnumerable<TEntity>> GetAllWithIncludesAsync(CancellationToken cancellationToken, Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            var includes = DbContext.GetIncludePaths(typeof(TEntity));
            return await GetAllAsync(cancellationToken, filter, orderBy, includes);
        }

        public virtual async Task AddAsync(TEntity entity, CancellationToken cancellationToken, bool saveNow = true)
        {
            Assert.NotNull(entity, nameof(entity));
            await Entities.AddAsync(entity, cancellationToken).ConfigureAwait(false);
            if (saveNow)
                await DbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveNow = true)
        {
            Assert.NotNull(entities, nameof(entities));
            await Entities.AddRangeAsync(entities, cancellationToken).ConfigureAwait(false);
            if (saveNow)
                await DbContext.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        public virtual async Task UpdateAsync(TEntity entity, CancellationToken cancellationToken, bool saveNow = true)
        {
            Assert.NotNull(entity, nameof(entity));
            Entities.Update(entity);
            if (saveNow)
                await DbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveNow = true)
        {
            Assert.NotNull(entities, nameof(entities));
            Entities.UpdateRange(entities);
            if (saveNow)
                await DbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task DeleteAsync(object id, CancellationToken cancellationToken, bool saveNow = true)
        {
            var entityToRemove = Entities.Find(id);
            await DeleteAsync(entityToRemove, cancellationToken, saveNow);
        }
        public virtual async Task DeleteAsync(TEntity entity, CancellationToken cancellationToken, bool saveNow = true)
        {
            Assert.NotNull(entity, nameof(entity));
            Entities.Remove(entity);
            if (saveNow)
                await DbContext.SaveChangesAsync(cancellationToken);
        }
        public virtual async Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken, bool saveNow = true)
        {
            Assert.NotNull(entities, nameof(entities));
            Entities.RemoveRange(entities);
            if (saveNow)
                await DbContext.SaveChangesAsync(cancellationToken);
        }
        #endregion

        #region Sync Methods

        public virtual IQueryable<TEntity> GetQuery()
        {
            return (IQueryable<TEntity>)this.DbContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> predicate)
        {
            return this.GetQuery().Where<TEntity>(predicate);
        }
        public virtual TEntity FindByKeys(params object[] keys)
        {
            return Entities.Find(keys);
        }

        public IEnumerable<TEntity> GetAllWithIncludes(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            var includes = DbContext.GetIncludePaths(typeof(TEntity));
            return GetAll(filter, orderBy, includes);
        }

        public virtual void Add(TEntity entity, bool saveNow = false)
        {
            Assert.NotNull(entity, nameof(entity));
            Entities.Add(entity);
            if (saveNow)
                DbContext.SaveChanges();
        }

        public virtual void AddRange(IEnumerable<TEntity> entities, bool saveNow = false)
        {
            Assert.NotNull(entities, nameof(entities));
            Entities.AddRange(entities);
            if (saveNow)
                DbContext.SaveChanges();
        }

        public virtual void Update(TEntity entity, bool saveNow = false)
        {
            Assert.NotNull(entity, nameof(entity));
            Entities.Update(entity);
            if (saveNow)
                DbContext.SaveChanges();
        }

        public virtual void UpdateRange(IEnumerable<TEntity> entities, bool saveNow = false)
        {
            Assert.NotNull(entities, nameof(entities));
            Entities.UpdateRange(entities);
            if (saveNow)
                DbContext.SaveChanges();
        }

        public virtual void Delete(object id, bool saveNow = false)
        {
            var entityToRemove = Entities.Find(id);
            Delete(entityToRemove, saveNow);
        }

        public virtual void Delete(TEntity entity, bool saveNow = false)
        {
            Assert.NotNull(entity, nameof(entity));
            Entities.Remove(entity);
            if (saveNow)
                DbContext.SaveChanges();
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entities, bool saveNow = false)
        {
            Assert.NotNull(entities, nameof(entities));
            Entities.RemoveRange(entities);
            if (saveNow)
                DbContext.SaveChanges();
        }
        #endregion

        #region Attach & Detach
        public virtual void Detach(TEntity entity)
        {
            Assert.NotNull(entity, nameof(entity));
            var entry = DbContext.Entry(entity);
            if (entry != null)
                entry.State = EntityState.Detached;
        }

        public virtual void Attach(TEntity entity)
        {
            Assert.NotNull(entity, nameof(entity));
            if (DbContext.Entry(entity).State == EntityState.Detached)
                Entities.Attach(entity);
        }
        #endregion

        #region Explicit Loading
        public virtual async Task LoadCollectionAsync<TProperty>(TEntity entity, Expression<Func<TEntity, IEnumerable<TProperty>>> collectionProperty, CancellationToken cancellationToken)
            where TProperty : class
        {
            Attach(entity);

            var collection = DbContext.Entry(entity).Collection(collectionProperty);
            if (!collection.IsLoaded)
                await collection.LoadAsync(cancellationToken).ConfigureAwait(false);
        }

        public virtual void LoadCollection<TProperty>(TEntity entity, Expression<Func<TEntity, IEnumerable<TProperty>>> collectionProperty)
            where TProperty : class
        {
            Attach(entity);
            var collection = DbContext.Entry(entity).Collection(collectionProperty);
            if (!collection.IsLoaded)
                collection.Load();
        }

        public virtual async Task LoadReferenceAsync<TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> referenceProperty, CancellationToken cancellationToken)
            where TProperty : class
        {
            Attach(entity);
            var reference = DbContext.Entry(entity).Reference(referenceProperty);
            if (!reference.IsLoaded)
                await reference.LoadAsync(cancellationToken).ConfigureAwait(false);
        }

        public virtual void LoadReference<TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> referenceProperty)
            where TProperty : class
        {
            Attach(entity);
            var reference = DbContext.Entry(entity).Reference(referenceProperty);
            if (!reference.IsLoaded)
                reference.Load();
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, IEnumerable<string> includeProperties = null)
        {
            IQueryable<TEntity> query = Entities;
            if (filter.IsNotNull())
            {
                query = query.Where(filter);
            }

            if (includeProperties.IsNotNull())
            {
                foreach (var property in includeProperties)
                {
                    query = query.Include(property);
                }
            }

            if (orderBy.IsNotNull())
            {
                return orderBy(query);
            }

            return query.ToList();
        }


        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null, IEnumerable<string> includeProperties = null)
        {
            IQueryable<TEntity> query = Entities;
            if (filter.IsNotNull())
            {
                query = query.Where(filter);
            }

            if (includeProperties.IsNotNull())
            {
                foreach (var property in includeProperties)
                {
                    query = query.Include(property);
                }
            }

            return query.FirstOrDefault();
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null, IEnumerable<string> includeProperties = null)
        {
            IQueryable<TEntity> query = Entities;
            if (filter.IsNotNull())
            {
                query = query.Where(filter);
            }

            if (includeProperties.IsNotNull())
            {
                foreach (var property in includeProperties)
                {
                    query = query.Include(property);
                }
            }

            return await query.FirstOrDefaultAsync();
        }

        // Stored Procedure Section

        public async Task<IEnumerable<TModel>> GetListSpAsync<TModel>(string procedureName, DynamicParameters parameters)
        {
            using (SqlConnection sqlcon = new SqlConnection(ConnectionStrings.SqlServerDefault))
            {
                await sqlcon.OpenAsync();
                return await sqlcon.QueryAsync<TModel>(procedureName, parameters,
                    commandType: CommandType.StoredProcedure);

            }
        }
        public IEnumerable<TModel> GetListSp<TModel>(string procedureName, DynamicParameters parameters = null)
        {
            using (SqlConnection sqlcon = new SqlConnection(ConnectionStrings.SqlServerDefault))
            {
                sqlcon.Open();
                return sqlcon.Query<TModel>(procedureName, parameters,
                    commandType: CommandType.StoredProcedure);

            }
        }
        public async Task ExecuteSpWithoutReturnAsync(string procedureName, DynamicParameters parameters = null)
        {
            using (SqlConnection sqlcon = new SqlConnection(ConnectionStrings.SqlServerDefault))
            {
                await sqlcon.OpenAsync();
                await sqlcon.ExecuteAsync(procedureName, parameters,
                    commandType: CommandType.StoredProcedure);

            }
        }
        public void ExecuteSpWithoutReturn(string procedureName, DynamicParameters parameters = null)
        {
            using (SqlConnection sqlcon = new SqlConnection(ConnectionStrings.SqlServerDefault))
            {
                sqlcon.Open();
                sqlcon.Execute(procedureName, parameters,
                   commandType: CommandType.StoredProcedure);

            }
        }
        public TModel ExecuteSpScalar<TModel>(string procedureName, DynamicParameters parameters = null)
        {
            using (SqlConnection sqlcon = new SqlConnection(ConnectionStrings.SqlServerDefault))
            {
                sqlcon.Open();
                return (TModel)Convert.ChangeType(sqlcon.ExecuteScalar<TModel>(procedureName, parameters,
                    commandType: CommandType.StoredProcedure), typeof(TModel));

            }
        }

        public async Task<TModel> ExecuteSpScalarAsync<TModel>(string procedureName, DynamicParameters parameters = null)
        {
            using (SqlConnection sqlcon = new SqlConnection(ConnectionStrings.SqlServerDefault))
            {
                await sqlcon.OpenAsync();
                return (TModel)Convert.ChangeType(await sqlcon.ExecuteScalarAsync<TModel>(procedureName, parameters,
                    commandType: CommandType.StoredProcedure), typeof(TModel));

            }
        }


        public void Dispose()
        {
           this.DbContext.Dispose();
           GC.SuppressFinalize(this);
        }





        #endregion
    }
}
