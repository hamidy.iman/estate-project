﻿using Estate.Infrastructure.DataModels.Entities.Base;

namespace Estate.Infrastructure.DataModels.Repositories.Base
{
    public interface IBaseRepository<TEntity>:IRepository<TEntity>
        where TEntity : class, IDbEntity
    {
       
     
    }
}