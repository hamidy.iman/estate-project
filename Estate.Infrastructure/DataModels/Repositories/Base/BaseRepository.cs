﻿using Estate.Infrastructure.Configurations;
using Estate.Infrastructure.DataModels.Entities.Base;

namespace Estate.Infrastructure.DataModels.Repositories.Base
{
    public class BaseRepository<TEntity> : Repository<TEntity,ApplicationDbContext>,IBaseRepository<TEntity>
        where TEntity : class, IDbEntity
    {
        public BaseRepository(ApplicationDbContext dbContext,IConnectionStrings connectionStrings) : base(dbContext,connectionStrings)
        {
        }
    }
}
