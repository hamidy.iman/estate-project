﻿using Estate.Infrastructure.Configurations;
using Estate.Infrastructure.DataModels.Entities;
using Estate.Infrastructure.DataModels.Repositories.Base;

namespace Estate.Infrastructure.DataModels.Repositories
{
    public class EstateRepository : BaseRepository<Entities.Estate>, IEstateRepository
    {
        public EstateRepository(ApplicationDbContext dbContext,IConnectionStrings connectionStrings)
            : base(dbContext,connectionStrings)
        {
        }

      
    }
}
