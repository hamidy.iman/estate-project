﻿using Estate.Infrastructure.DataModels.Entities;
using Estate.Infrastructure.DataModels.Repositories.Base;

namespace Estate.Infrastructure.DataModels.Repositories
{
    public interface IEstateRepository : IBaseRepository<Entities.Estate>
    {
        
    }
}