﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Estate.Infrastructure.DataModels.Entities;
using Estate.Shared.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Estate.Infrastructure.DataModels.Initializer
{
    public class DbInitializer:IDbInitializer
    {
        private readonly ApplicationDbContext DbContext;
       
        public DbInitializer(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }
       
        public  void Initialize()
        {


            CreateInitialData().Wait();


        }

        private  async Task CreateInitialData()
        {
            //var context = serviceProvider.GetRequiredService<ApplicationDbContext>();

            try
            {
                var migrationcount = (await DbContext.Database.GetPendingMigrationsAsync()).Count();
                if (migrationcount > 0)
                   await DbContext.Database.MigrateAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            await DbContext.Database.EnsureCreatedAsync();

            var savedowner=await DbContext.EstateOwners.FirstOrDefaultAsync(p => p.MobileNumber == "09168226095");

           if (savedowner.IsNull())
           {
               await DbContext.EstateOwners.AddAsync(new EstateOwner()
               {

                   Family = "Hamidi",
                   Name = "Iman",
                   MobileNumber = "09168226095",

               });
             await  DbContext.SaveChangesAsync();
           }
            

        }
    }
}
