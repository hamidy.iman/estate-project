﻿namespace Estate.Infrastructure.DataModels.Initializer
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
