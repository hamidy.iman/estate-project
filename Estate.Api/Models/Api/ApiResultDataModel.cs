﻿using System.Linq;
using Estate.Shared.Enums;
using Estate.Shared.Resources;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static Estate.Shared.Enums.Enums;

namespace Estate.Api.Models.Api
{

    public class ApiResultDataModel<TData> : ApiResultDataModel
        where TData : class
    {

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TData Data { get; set; }

        public static implicit operator ApiResultDataModel<TData>(TData value)
        {
            return new ApiResultDataModel<TData>()
            {
                Success = true,
                Message = ResponseStatusCodeType.Ok.GetDisplayValue(typeof(Messages)),
                Data = value,
                StatusCodeType = ResponseStatusCodeType.Ok,
                StatusCode = (int)ResponseStatusCodeType.Ok,
            }
            ;
        }

        public static implicit operator ApiResultDataModel<TData>(OkObjectResult value)
        {
            return new ApiResultDataModel<TData>()
            {
                Success = true,
                Data = (TData)value.Value,
                Message = ResponseStatusCodeType.Ok.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.Ok,
                StatusCode = (int)ResponseStatusCodeType.Ok,
            };

        }

        public static implicit operator ApiResultDataModel<TData>(ObjectResult result)
        {
            return new ApiResultDataModel<TData>()
            {
                Success = true,
                Data = (TData)result.Value,
                Message = ResponseStatusCodeType.Ok.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.Ok,
                StatusCode = (int)ResponseStatusCodeType.Ok,
            };

        }

        public static implicit operator ApiResultDataModel<TData>(NotFoundObjectResult value)
        {
            return new ApiResultDataModel<TData>()
            {
                Success = false,
                Data = null,
                Message = ResponseStatusCodeType.NotFound.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.NotFound,
                StatusCode = (int)ResponseStatusCodeType.NotFound,
            };

        }

        public static implicit operator ApiResultDataModel<TData>(BadRequestResult value)
        {
            return new ApiResultDataModel<TData>()
            {
                Success = false,
                Data = null,
                Message = ResponseStatusCodeType.BadRequest.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.BadRequest,
                StatusCode = (int)ResponseStatusCodeType.BadRequest,
            };

        }

        public static implicit operator ApiResultDataModel<TData>(AcceptedResult value)
        {
            return new ApiResultDataModel<TData>()
            {
                Success = false,
                Data = null,
                Message = ResponseStatusCodeType.Accepted.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.Accepted,
                StatusCode = (int)ResponseStatusCodeType.Accepted,
            };

        }

        public static implicit operator ApiResultDataModel<TData>(BadRequestObjectResult result)
        {
            var message = result.Value.ToString();
            if (result.Value is SerializableError errors)
            {
                var errorMessages = errors.SelectMany(p => (string[])p.Value).Distinct();
                message = string.Join(" | ", errorMessages);
            }
            return new ApiResultDataModel<TData>()
            {
                Success = false,
                Data = null,
                Message = message,
                StatusCodeType = ResponseStatusCodeType.BadRequest,
                StatusCode = (int)ResponseStatusCodeType.BadRequest,
            };
        }

      


    }

    public class ApiResultDataModel
    {
        public bool Success { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        public int StatusCode
        {
            get
            {
                return (int) StatusCodeType;
            }
            set
            {
                StatusCode = value;
            }
        }

        public ResponseStatusCodeType StatusCodeType { get; set; }

        public static implicit operator ApiResultDataModel(OkResult value)
        {
            return new ApiResultDataModel()
            {
                Success = true,
                Message = ResponseStatusCodeType.Ok.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.Ok,
            };

        }

        public static implicit operator ApiResultDataModel(NotFoundResult value)
        {
            return new ApiResultDataModel()
            {
                Success = false,
                Message = ResponseStatusCodeType.NotFound.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.NotFound,
            };

        }

        public static implicit operator ApiResultDataModel(BadRequestResult value)
        {
            return new ApiResultDataModel()
            {
                Success = false,
                Message = ResponseStatusCodeType.BadRequest.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.BadRequest,
            };

        }

        public static implicit operator ApiResultDataModel(AcceptedResult value)
        {
            return new ApiResultDataModel()
            {
                Success = true,
                Message = ResponseStatusCodeType.Accepted.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.Accepted,
            };

        }

        public static implicit operator ApiResultDataModel(CreatedResult value)
        {
            return new ApiResultDataModel()
            {
                Success = true,
                Message = ResponseStatusCodeType.Created.GetDisplayValue(typeof(Messages)),
                StatusCodeType = ResponseStatusCodeType.Created,
            };

        }

        public static implicit operator ApiResultDataModel(BadRequestObjectResult result)
        {
            var message = result.Value.ToString();
            if (result.Value is SerializableError errors)
            {
                var errorMessages = errors.SelectMany(p => (string[])p.Value).Distinct();
                message = string.Join(" | ", errorMessages);
            }
            return new ApiResultDataModel()
            {
                Success = false,
                Message = message,
                StatusCodeType = ResponseStatusCodeType.BadRequest,
            };
        }

       

    }
}
