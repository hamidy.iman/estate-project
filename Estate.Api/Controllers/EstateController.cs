﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Estate.Api.ActionFilters;
using Estate.Api.Models.Api;
using Estate.Services.Models;
using Estate.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Estate.Api.Controllers
{
    [Route("api/estate")]
    [ApiController]
    [ApiResultActionFilter]
    // CRUD Operation On Estate Table 
    public class EstateController : ControllerBase
    {
        private readonly IEstateService estateService;
        public EstateController(IEstateService estateService)
        {
            this.estateService = estateService;
        }


        [HttpGet]
        public async Task<ActionResult<List<EstateModel>>> Get(CancellationToken cancellationToken)
        {
            try
            {
                var estates = await estateService.GetAllEstatesAsync(cancellationToken);
                return Ok(estates);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
           
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<EstateModel>> Get(string id, CancellationToken cancellationToken)
        {
            try
            {
                var estate = await estateService.GetEstateAsync(id, cancellationToken);
                return estate;
            }
            catch (InvalidOperationException e)
            {
                return BadRequest("شناسه وارد شده برای ملک صحیح نمی باشد");
            }
            catch (Exception e)
            {
               return NotFound(e.Message);
            }
           
           
        }

        [HttpPost("create")]
        public async Task<ApiResultDataModel> Create(EstateCreateUpdateModel estateModel, CancellationToken cancellationToken)
        {

            try
            {
                await estateService.CreateEstateAsync(estateModel, cancellationToken);
                return Created("", estateModel);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest("شناسه وارد شده برای ملک صحیح نمی باشد");
            }
            catch (DbUpdateException e)
            {
                return BadRequest("اطلاعات وارد شده صحیح نمی باشد");
            }
            catch (Exception e)
            {
                return BadRequest("خطایی در عملیات رخ داد");
            }


        }

        [HttpPut("update")]
        public async Task<ActionResult<ApiResultDataModel>> Update(EstateCreateUpdateModel estateModel, CancellationToken cancellationToken)
        {

            try
            {
                await estateService.UpdateEstateAsync(estateModel, cancellationToken);
                return Ok();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest("شناسه وارد شده برای ملک صحیح نمی باشد");
            }
            catch (DbUpdateException e)
            {
               return NotFound("مالک ملک مورد نظر یافت نشد");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }


        }

        [HttpDelete("delete")]
        public async Task<ActionResult<ApiResultDataModel>> Delete(string id, CancellationToken cancellationToken)
        {
            try
            {
                await estateService.DeleteEstateAsync(id, cancellationToken);
                return Ok();
            }
            catch (InvalidOperationException e)
            {
                return BadRequest("شناسه وارد شده برای ملک صحیح نمی باشد");
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

    }
}
