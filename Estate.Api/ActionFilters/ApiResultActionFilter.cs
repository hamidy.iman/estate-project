﻿using System;
using System.Globalization;
using System.Linq;
using Estate.Api.Models.Api;
using Estate.Shared.Enums;
using Estate.Shared.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Estate.Api.ActionFilters
{
    public class ApiResultActionFilterAttribute:ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {

            if (context.Result is OkObjectResult okObjectResult)
            {
                var apiResult = new ApiResultDataModel<object>()
                {
                    Success = true,
                    StatusCodeType = Enums.ResponseStatusCodeType.Ok,
                    Message = Enums.ResponseStatusCodeType.Ok.GetDisplayValue(typeof(Messages)),
                    Data = okObjectResult.Value

                };
                context.Result = new JsonResult(apiResult);
            }
            else if (context.Result is OkResult okResult)
            {
                var apiResult = new ApiResultDataModel()
                {
                    StatusCodeType = Enums.ResponseStatusCodeType.Ok,
                    Message =  Enums.ResponseStatusCodeType.Ok.GetDisplayValue(typeof(Messages)),
                    Success = true
                };
                context.Result = new JsonResult(apiResult);
            }
            else if (context.Result is BadRequestResult badRequestResult)
            {
                var apiResult = new ApiResultDataModel()
                {
                    StatusCodeType = Enums.ResponseStatusCodeType.BadRequest,
                    Message = Enums.ResponseStatusCodeType.BadRequest.GetDisplayValue(typeof(Messages)),
                    Success = false
                };
                context.Result = new JsonResult(apiResult);
            }
            else if (context.Result is BadRequestObjectResult badRequestObjectResult)
            {
                var message = badRequestObjectResult.Value.ToString();
                if (badRequestObjectResult.Value is SerializableError errors)
                {
                    var errorMessages = errors.SelectMany(p => (string[])p.Value).Distinct();
                    message = string.Join(" | ", errorMessages);
                }
                if (badRequestObjectResult.Value is ValidationProblemDetails error)
                {
                    var errorMessages = error.Errors.SelectMany(p => (string[])p.Value).Distinct();
                    message = string.Join(" | ", errorMessages);
                }
                var apiResult = new ApiResultDataModel()
                {
                    StatusCodeType = Enums.ResponseStatusCodeType.BadRequest,
                    Message = message,
                    Success = false
                };
                context.Result = new JsonResult(apiResult);
            }
          
            else if (context.Result is NotFoundResult notFoundResult)
            {
                var apiResult = new ApiResultDataModel()
                {
                    StatusCodeType = Enums.ResponseStatusCodeType.NotFound,
                    Message = Enums.ResponseStatusCodeType.NotFound.GetDisplayValue(typeof(Messages)),
                    Success = false
                };
                context.Result = new JsonResult(apiResult);
            }
            else if (context.Result is NotFoundObjectResult notFoundObjectResult)
            {
                var apiResult = new ApiResultDataModel<object>()
                {
                    Success = false,
                    StatusCodeType = Enums.ResponseStatusCodeType.NotFound,
                    Message = notFoundObjectResult.Value.ToString()??Enums.ResponseStatusCodeType.NotFound.GetDisplayValue(typeof(Messages)),
                    Data = null

                };
                context.Result = new JsonResult(apiResult);
            }
            else if (context.Result is CreatedResult createdResult)
            {
                var apiResult = new ApiResultDataModel<object>()
                {
                    Success = true,
                    StatusCodeType = Enums.ResponseStatusCodeType.Created,
                    Message = Enums.ResponseStatusCodeType.Created.GetDisplayValue(typeof(Messages)),
                    Data = createdResult.Value

                };
                context.Result = new JsonResult(apiResult);
            }
            else if (context.Result is ObjectResult objectResult && objectResult.StatusCode == null
                && !(objectResult.Value is ApiResultDataModel))
            {
                try
                {
                    var apiResult = new ApiResultDataModel<object>();

                    
                        apiResult.Success = true;
                        apiResult.StatusCodeType = Enums.ResponseStatusCodeType.Ok;
                        apiResult.Message = Enums.ResponseStatusCodeType.Ok.GetDisplayValue(typeof(Messages));
                        apiResult.Data = objectResult.Value;

                    
                    context.Result = new JsonResult(apiResult);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
               
               
            }
           
            base.OnResultExecuting(context);
        }

       
    }
}
