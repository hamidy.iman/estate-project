﻿using Estate.Infrastructure.DataModels.Initializer;
using Estate.Services.Services;
using Estate.Services.Services.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Estate.Services.Extensions
{
   public static class ServiceCollectionExtension
    {

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IEstateService, EstateService>();
            services.AddScoped<IEstateOwnerService, EstateOwnerService>();
            services.AddScoped(typeof(IBaseService<>), (typeof(BaseService<>)));
            services.AddScoped<IDbInitializer, DbInitializer>();
            return services;
        }

    }
}
