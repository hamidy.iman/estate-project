﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Estate.Shared.DataAnnotations;
using Estate.Shared.Resources;
using Estate.Shared.Utilities;

namespace Estate.Services.Models
{
    public class EstateOwnerModel : IValidatableObject
    {

        public Guid Id { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(128, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Name { get; set; }


        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(8, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Family { get; set; }

        [StringLength(512, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [PersianMobileNumberValidator(ErrorMessageResourceName = "MobileNumberFormatError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string MobileNumber { get; set; }

        [StringLength(512, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [PersianPhoneNumberValidator(ErrorMessageResourceName = "PhoneNumberFormatError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string PhoneNumber { get; set; }


        public List<EstateModel> Estates { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Name.Equals("test", StringComparison.OrdinalIgnoreCase))
                yield return new ValidationResult("نام نمیتواند Test باشد", new[] { nameof(Name) });
            if (Name.ExtractDigits().Length>0)
                yield return new ValidationResult("نام نمی تواند حاوی عدد باشد", new[] { nameof(Name) });
            
        }
    }
}
