﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Estate.Shared.Enums;
using Estate.Shared.Resources;
using Estate.Shared.Utilities;

namespace Estate.Services.Models
{
    public class EstateCreateUpdateModel : IValidatableObject
    {

        public Guid Id { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(8, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Number { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Range(minimum: 0, maximum: Double.MaxValue, ErrorMessageResourceName = "NumberRangeError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public double Area { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public Enums.EstateLocation Location { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [StringLength(128, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Name { get; set; }
        public string aaa { get; set; }

        [StringLength(512, ErrorMessageResourceName = "StringLengthError", ErrorMessageResourceType = typeof(ValidationMessages))]
        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public string Address { get; set; }

        [Required(ErrorMessageResourceName = "FieldRequiredError", ErrorMessageResourceType = typeof(ValidationMessages))]
        public Guid OwnerId { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Name.Equals("test", StringComparison.OrdinalIgnoreCase))
                yield return new ValidationResult("نام نمیتواند Test باشد", new[] { nameof(Name) });
            if (!Number.IsNumeric())
                yield return new ValidationResult("شماره ملک باید عددی باشد", new[] { nameof(Name) });

        }
    }
}
