﻿using System.Security.Claims;
using System.Threading.Tasks;
using Estate.Infrastructure.DataModels.Entities.Base;
using Estate.Infrastructure.DataModels.Repositories.Base;
using Estate.Services.Models;
using Microsoft.AspNetCore.Http;

namespace Estate.Services.Services.Base
{
    public class BaseService<TEntity> :IBaseService<TEntity> where TEntity: class, IDbEntity
    {
       
        protected IBaseRepository<TEntity> Repository { get; set; }
        private IHttpContextAccessor httpContextAccessor;

        public BaseService(IHttpContextAccessor httpContextAccessor,IBaseRepository<TEntity> repository)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.Repository = repository;

        }
      
    }
}
