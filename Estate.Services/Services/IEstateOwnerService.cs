﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Estate.Services.Models;

namespace Estate.Services.Services
{
    public interface IEstateOwnerService
    {
        Task CreateEstateOwnerAsync(EstateOwnerModel clientModel,CancellationToken cancelationToken);
        Task UpdateEstateOwnerAsync(EstateOwnerModel clientModel, CancellationToken cancelationToken);
        Task DeleteEstateOwnerAsync(string id, CancellationToken cancelationToken);
        Task<List<EstateOwnerModel>> GetAllEstateOwnersAsync(CancellationToken cancellationToken);
        Task<EstateOwnerModel> GetEstateOwnerAsync(string id, CancellationToken cancellationToken);
    }
}
