﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Estate.Services.Models;

namespace Estate.Services.Services
{
    public interface IEstateService
    {
        Task CreateEstateAsync(EstateCreateUpdateModel clientModel,CancellationToken cancelationToken);
        Task UpdateEstateAsync(EstateCreateUpdateModel clientModel,CancellationToken cancelationToken);
        Task DeleteEstateAsync(string id,CancellationToken cancelationToken);
        Task<List<EstateModel>> GetAllEstatesAsync(CancellationToken cancellationToken);
        Task<EstateModel> GetEstateAsync(string id,CancellationToken cancellationToken);
    }
}
