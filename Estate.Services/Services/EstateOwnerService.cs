﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Estate.Infrastructure.DataModels.Entities;
using Estate.Infrastructure.DataModels.Repositories;
using Estate.Services.Models;
using Estate.Services.Services.Base;
using Estate.Shared.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Estate.Services.Services
{
    public class EstateOwnerService : BaseService<EstateOwner>, IEstateOwnerService
    {

        public EstateOwnerService(IHttpContextAccessor httpContextAccessor, IEstateOwnerRepository repository) :
            base(httpContextAccessor, repository)
        {

        }

        public async Task CreateEstateOwnerAsync(EstateOwnerModel estateOwnerModel, CancellationToken cancellationToken)
        {
            var estateOwner = new EstateOwner();
            estateOwnerModel.CopyPropertiesTo(estateOwner);
            using (Repository)
            {
                var savedentity = await Repository.GetQuery(p => p.MobileNumber == estateOwnerModel.MobileNumber).FirstOrDefaultAsync();
                if (savedentity.IsNotNull())
                    throw new DbUpdateException("مالک با این شماره موبایل قبلا ثبت شده است");

                await Repository.AddAsync(savedentity, cancellationToken, true);
            }

        }

        public async Task DeleteEstateOwnerAsync(string id, CancellationToken cancelationToken)
        {
            using (Repository)
            {
                var savedentity = await Repository.GetQuery(p => p.Id == Guid.Parse(id)).FirstOrDefaultAsync();
                if (savedentity.IsNotNull())
                {

                    await Repository.DeleteAsync(savedentity, cancelationToken, true);

                }
                else
                {
                    throw new NullReferenceException("مالکی با این شناسه یافت نشد");
                }


            }
        }
    

        public async Task<EstateOwnerModel> GetEstateOwnerAsync(string id, CancellationToken cancellationToken)
        {
            var estateowner = await Repository.GetQuery(p => p.Id == Guid.Parse(id)).Include(p => p.Estates).FirstOrDefaultAsync();

            if (estateowner.IsNull())
            {
                throw new NullReferenceException("مالکی با این شناسه یافت نشد");
            }
            var estateownermodel = new EstateOwnerModel()
            {
                Name = estateowner.Name,
                MobileNumber = estateowner.MobileNumber,
                PhoneNumber = estateowner.PhoneNumber,
                Family = estateowner.Family,
                Id = estateowner.Id,
                Estates = (from estate in estateowner.Estates
                           select new EstateModel
                           {
                               Name = estate.Name,
                               OwnerId = estate.OwnerId,
                               Id = estate.Id,
                               Address = estate.Address,
                               Number = estate.Number

                           }).ToList()
            };

            return estateownermodel;
        }

        public async Task<List<EstateOwnerModel>> GetAllEstateOwnersAsync(CancellationToken cancellationToken)
        {
            var estateowners = Repository.GetQuery().Include(p => p.Estates);
            var estateownersmodel = await (from estateowner in estateowners
                                           select new EstateOwnerModel()
                                           {
                                               Name = estateowner.Name,
                                               Family = estateowner.Family,
                                               Id = estateowner.Id,
                                               MobileNumber = estateowner.MobileNumber,
                                               PhoneNumber = estateowner.PhoneNumber,
                                               Estates = (from estate in estateowner.Estates
                                                          select new EstateModel()
                                                          {
                                                              Name = estate.Name,
                                                              Number = estate.Number,
                                                              OwnerId = estate.OwnerId,
                                                              Address = estate.Address,
                                                              Id = estate.Id
                                                          }).ToList()
                                           }).ToListAsync();

            return estateownersmodel;
        }

        public async Task UpdateEstateOwnerAsync(EstateOwnerModel estateModel, CancellationToken cancelationToken)
        {
            using (Repository)
            {
                var savedentity = await Repository.GetQuery(p => p.Id == estateModel.Id).FirstOrDefaultAsync();
                if (savedentity.IsNotNull())
                {
                    estateModel.CopyPropertiesTo(savedentity);
                    await Repository.UpdateAsync(savedentity, cancelationToken, true);

                }
                else
                {
                    throw new NullReferenceException("ملکی با این شناسه یافت نشد");
                }


            }
        }
    }
}
