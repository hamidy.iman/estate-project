﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Estate.Infrastructure.DataModels.Repositories;
using Estate.Services.Models;
using Estate.Services.Services.Base;
using Estate.Shared.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Estates = Estate.Infrastructure.DataModels.Entities.Estate;

namespace Estate.Services.Services
{
    public class EstateService : BaseService<Infrastructure.DataModels.Entities.Estate>, IEstateService
    {

        private readonly IEstateOwnerRepository estateOwnerRepository;
        public EstateService(IHttpContextAccessor httpContextAccessor, IEstateRepository repository
        ,IEstateOwnerRepository estateOwnerRepository) :
            base(httpContextAccessor, repository)
        {
            this.estateOwnerRepository = estateOwnerRepository;
        }

        public async Task CreateEstateAsync(EstateCreateUpdateModel estateModel, CancellationToken cancellationToken)
        {
            var estate = new Infrastructure.DataModels.Entities.Estate();
            estateModel.CopyPropertiesTo(estate);
            using (Repository)
            {
                var savedentity = await Repository.GetQuery(p => p.Number == estateModel.Number || p.Name==estateModel.Name).FirstOrDefaultAsync();
                if (savedentity.IsNotNull())
                    throw new DbUpdateException("ملک با این شماره یا نام قبلا ثبت شده است");

                await Repository.AddAsync(estate, cancellationToken, true);
            }

        }

        public async Task DeleteEstateAsync(string id, CancellationToken cancelationToken)
        {
            using (Repository)
            {
                var savedentity = await Repository.GetQuery(p => p.Id == Guid.Parse(id)).FirstOrDefaultAsync();
                if (savedentity.IsNotNull())
                {
                   
                    await Repository.DeleteAsync(savedentity, cancelationToken, true);

                }
                else
                {
                    throw new Exception("ملکی با این شناسه یافت نشد");
                }


            }
        }

        public async Task<List<EstateModel>> GetAllEstatesAsync(CancellationToken cancellationToken)
        {
            var estates = Repository.GetQuery().Include(p=>p.EstateOwner);
            var estatesmodel =await (from estate in estates
                select new EstateModel()
                {
                    Name = estate.Name,
                    Number = estate.Number,
                    Address = estate.Address,
                    Id = estate.Id,
                    OwnerId = estate.OwnerId,
                    Area = estate.Area,
                    Location = estate.Location,
                    Owner=new EstateOwnerModel()
                    {
                        Name = estate.EstateOwner.Name,
                        MobileNumber = estate.EstateOwner.MobileNumber,
                        PhoneNumber = estate.EstateOwner.PhoneNumber,
                        Id = estate.EstateOwner.Id,
                        Family = estate.EstateOwner.Family
                    }
                }).ToListAsync();

            return estatesmodel;
        }

        public async Task<EstateModel> GetEstateAsync(string id, CancellationToken cancellationToken)
        {
            var estate =await Repository.GetQuery(p=>p.Id==Guid.Parse(id)).Include(p => p.EstateOwner).FirstOrDefaultAsync();

            if (estate.IsNull())
            {
                throw new Exception("ملکی با این شناسه یافت نشد");
            }
            var estatemodel = new EstateModel()
                {
                    Name = estate.Name,
                    Number = estate.Number,
                    Address = estate.Address,
                    Id = estate.Id,
                    OwnerId = estate.OwnerId,
                    Area = estate.Area,
                    Location = estate.Location,
                Owner = new EstateOwnerModel()
                    {
                        Name = estate.EstateOwner.Name,
                        MobileNumber = estate.EstateOwner.MobileNumber,
                        PhoneNumber = estate.EstateOwner.PhoneNumber,
                        Id = estate.EstateOwner.Id,
                        Family = estate.EstateOwner.Family
                    }
                };

            return estatemodel;
        }

        public async Task UpdateEstateAsync(EstateCreateUpdateModel estateModel, CancellationToken cancelationToken)
        {
            using (Repository)
            {
                var savedentity = await Repository.GetQuery(p => p.Id==estateModel.Id).FirstOrDefaultAsync();
                if (savedentity.IsNotNull())
                {
                    estateModel.CopyPropertiesTo(savedentity);
                    await Repository.UpdateAsync(savedentity, cancelationToken, true);

                }
                else
                {
                    throw new Exception("ملکی با این شناسه یافت نشد");
                }

               
            }
        }
    }
}
